# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from datetime import datetime
from trytond.exceptions import UserError
from trytond.i18n import gettext


def string_to_date(text, patterns=('%d/%m/%Y', '%Y-%m-%d')):
    for pattern in patterns:
        try:
            return datetime.strptime(text, pattern)
        except ValueError:
            continue
    raise UserError(gettext('account_bank_statement.invalid_date',
        date=text))


def string_to_number(text, decimal_separator='.', thousands_separator=','):
    text = text.replace(thousands_separator, '')
    if decimal_separator != '.':
        text = text.replace(decimal_separator, '.')
    try:
        return Decimal(text)
    except ValueError:
        raise UserError(gettext('account_bank_statement.invalid_number',
            number=text))
