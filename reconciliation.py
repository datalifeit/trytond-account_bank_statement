# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import Index, ModelSQL, ModelView, fields
from trytond.modules.currency.fields import Monetary
from trytond.pool import Pool
from trytond.rpc import RPC
from trytond.transaction import Transaction


class AccountBankReconciliation(ModelView, ModelSQL):
    'Account Bank Reconciliation'
    __name__ = 'account.bank.reconciliation'

    amount = Monetary('Amount', digits='currency', currency='currency',
        required=True)
    currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'on_change_with_currency')
    move_line = fields.Many2One('account.move.line', 'Move Line',
        required=True, readonly=True, ondelete='CASCADE')
    move_date = fields.Function(fields.Date('Move date'),
        'get_move_field', searcher='search_move_field')
    move_origin = fields.Function(
        fields.Reference('Move origin', selection='get_move_origin'),
        'get_move_field', searcher='search_move_field')
    move_description = fields.Function(
        fields.Char('Move description'), 'get_move_field',
        searcher='search_move_field')
    bank_statement_line = fields.Many2One('account.bank.statement.line',
        'Bank Statement Line')

    @classmethod
    def __setup__(cls):
        super(AccountBankReconciliation, cls).__setup__()
        table = cls.__table__()
        cls.__rpc__.update({
                'get_move_origin': RPC(),
        })
        cls._sql_indexes.add(
            Index(table, (table.bank_statement_line, Index.Equality()))
            )

    @classmethod
    def write(cls, *args):
        super(AccountBankReconciliation, cls).write(*args)
        actions = iter(args)
        if not Transaction().context.get('from_account_bank_statement_line'):
            for lines, _ in zip(actions, actions):
                move_lines = set([x.move_line for x in lines])
                for move_line in move_lines:
                    move_line.check_bank_lines()

    @classmethod
    def delete(cls, lines):
        if not Transaction().context.get('from_account_bank_statement_line'):
            unallowed = [x for x in lines if x.bank_statement_line]
            if unallowed:
                line = unallowed[0]
                raise UserError(gettext(
                    'account_bank_statement.delete_reconciled',
                    amount=line.amount,
                    statement_line=line.bank_statement_line.rec_name))

        super(AccountBankReconciliation, cls).delete(lines)

    @fields.depends('move_line', '_parent_move_line.account')
    def on_change_with_currency(self, name=None):
        if self.move_line and self.move_line.currency:
            return self.move_line.account.company.currency.id

    @classmethod
    def get_move_origin(cls):
        Move = Pool().get('account.move')
        return Move.get_origin()

    def get_move_field(self, name):
        field = getattr(self.__class__, name)
        if name.startswith('move_') and name != 'move_origin':
            name = name[5:]
        value = getattr(self.move_line, name)
        if isinstance(value, ModelSQL):
            if field._type == 'reference':
                return str(value)
            return value.id
        return value

    @classmethod
    def search_move_field(cls, name, clause):
        if name.startswith('move_') and name != 'move_origin':
            name = name[5:]
        return [('move_line.' + name,) + tuple(clause[1:])]

    def _order_move_field(name):
        def move_field(tables):
            pool = Pool()
            Move = pool.get('account.move.line')
            field = Move._fields[name]
            table, _ = tables[None]
            move_tables = tables.get('move')
            if move_tables is None:
                move = Move.__table__()
                move_tables = {
                    None: (move, move.id == table.move_line),
                    }
                tables['move'] = move_tables
            return field.convert_order(name, move_tables, Move)
        return staticmethod(move_field)

    order_move_date = _order_move_field('date')
    order_move_origin = _order_move_field('move_origin')
    order_move_description = _order_move_field('description')
